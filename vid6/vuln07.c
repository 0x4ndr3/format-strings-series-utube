/*

Author: 0x4ndr3
Explanation and exploit gen at utube video: https://youtu.be/yV4qo8Cu5U0

*/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>

char password[] = "Nz!Tvq4s!Q5ttx1se"; // password = My Sup3r P4ssw0rd

void grant_access(){
    printf("\n[+] Access granted to super terminal:\n");
    system("/bin/sh");
}

void main(){
        char buffer[512];
        for(char *c = password ; *c ; ++c)
               *c -= 1;
        printf("Insert password: ");
        gets(buffer,sizeof(buffer),stdin);
        printf("Validating password: ");
        printf(buffer);
        if (!strcmp(password,buffer))
                grant_access();
        else
                printf("\n[-] Access denied to pwd = %s \n",buffer);
}