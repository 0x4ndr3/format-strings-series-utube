/*

Author: 0x4ndr3
Explanation and exploit gen at utube video: https://www.youtube.com/watch?v=YFeegMXyhtw

*/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<stdbool.h>

char password[] = "Nz!Tvq4s!Q5ttx1se"; // password = My Sup3r P4ssw0rd

void main(){
    char buffer[512];
    bool* equal = malloc(1); // pointer to result is placed in stack
    for(char *c = password ; *c ; ++c)
        *c -= 1;
    printf("Insert password: ");
    gets(buffer,sizeof(buffer),stdin);
    *equal = !strcmp(password,buffer); // notice the vuln function has to be after the setting of equal
    printf("Validating password: ");
    printf(buffer);
    if (*equal)
        printf("\n[+] Access granted\n");
    else
        printf("\n[-] Access denied\n");
    free(equal);
}