/*

Author: 0x4ndr3
Explanation and exploit gen at utube video: https://www.youtube.com/watch?v=JhdHE4XmevI

*/
#include <stdio.h>
#include <stdarg.h>

int min(int count, ...)
{
        int min, next_arg;

        va_list ap;
        va_start(ap, count);
        min = va_arg(ap, int);

        for (int i = 2; i <= count; i++)
                if ((next_arg = va_arg(ap, int)) < min)
                        min = next_arg;

        va_end(ap);
        return min;
}

int main()
{
        int count = 3;
        printf("Min value is %d\n", min(count, 78, 700, 35));
        return 0;
}