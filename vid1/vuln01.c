/*

Author: 0x4ndr3
Explanation and exploit gen at utube video: https://www.youtube.com/watch?v=JhdHE4XmevI

*/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>

void main(){
        char buffer[512];
        char password[] = "Nz!Tvq4s!Q5ttx1se"; // password = My Sup3r P4ssw0rd
        for(char* c = password ; *c ; ++c)
                *c -= 1;
        printf("Insert password: ");
        gets(buffer,sizeof(buffer),stdin);
        printf("Validating password: ");
        printf(buffer); // vulnerable
        if(!strcmp(buffer,password))
                printf("\n[+] Access granted\n");
        else
                printf("\n[-] Access denied to pwd = %s\n",buffer);
}