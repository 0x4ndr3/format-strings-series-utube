/*

Author: 0x4ndr3
Explanation and exploit gen at utube video: https://youtu.be/n9Nm6CGppJs

*/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<stdbool.h>

char password[] = "Nz!Tvq4s!Q5ttx1se"; // password = My Sup3r P4ssw0rd

void main(){
        char buffer[512];
        int attempts = 2;
        bool diff = true;
        for(char *c = password ; *c ; ++c)
               *c -= 1;
        do {
                printf("\nInsert password:\n");
                gets(buffer,sizeof(buffer),stdin);
                printf("Validating password: ");
                printf(buffer);
                diff = strcmp(buffer,password);
        } while(diff && --attempts);
        if(diff)
                printf("\n[-] Access denied!\n");
        else
                printf("\n[+] Access granted!\n");
}