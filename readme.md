# Format Strings series on **jmpcallpop**

Youtube channel: http://www.youtube.com/channel/UCWEsU7ckmiPQ8nXR_cJ8DJA

## TOC

1. Video #1: Exploit Development | Format Strings Series 1/6 - Intro and dumping sensitive data
2. Video #2: Exploit Development | Format Strings Series 2/6 - Redirecting code flow
3. Video #3: Exploit Development | Format Strings Series 3/6 - More control over the writing process
4. Video #4: Exploit Development | Format Strings Series 4/6 - Rewriting the GOT table
5. Video #5: Exploit Development | Format Strings Series 5/6 - When you have more than one vulnerable printf - bypassing ASLR (not bruteforce)
6. Video #6: Exploit Development | Format Strings Series 6/6 - x64 exploitation + Final thoughts

## Contact

[twitter](https://twitter.com/0x4ndr3)
[linkedin](https://www.linkedin.com/in/aflima/)
